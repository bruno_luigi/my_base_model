<?php

/*
 * 
 * Versão 0.6
 * 
 */

class MY_Base_Model extends MY_Model{
		private $atributos;
		
		//
		protected $admin_usage = false;
		protected $default_admin_status = array(self::STATUS_ATIVO, self::STATUS_INATIVO, self::STATUS_EXCLUIDO);
		protected $default_public_status = array(self::STATUS_ATIVO);
	
		const BASE_MODEL_CLASSNAME = 'MY_Base_Model';
		
		//Configurações da coluna padrão Status
		const STATUS_PADRAO = self::STATUS_INATIVO;
		const STATUS_ATIVO = 'Ativo';
		const STATUS_INATIVO = 'Inativo';
		const STATUS_EXCLUIDO = 'Excluido';
		
		protected $status_validos = array('Ativo' => self::STATUS_ATIVO, 'Inativo' => self::STATUS_INATIVO, 'Excluido' => self::STATUS_EXCLUIDO);
		
		public function __construct($parametros=array()){
			$this->inicializaAtributos();
			$this->atualizaAtributos($parametros);

			//Chama construtor da classe MY_Model
			parent::__construct();
		}

		public function _has_status_column(){
			$this->db->list_fields($this->_table);
		}
		
		public function get_default_status(){
		if($this->admin_usage)
			return $this->default_admin_status;
		else
			return $this->default_public_status;
		}
		
		public function set_admin_usage($admin_usage, $valid_statuses=array()){
			$this->admin_usage=$admin_usage;
			if(is_array($valid_statuses) && $valid_statuses){
				$this->default_admin_status = $valid_statuses;
			}
		}

		public function destroy($id=null){
			if($id == null){
				$id = $this->id;
			}
			
			$this->db->where('id', $id);
			
			$data_atualizacao = date("Y-m-d H:i:s");
			
			if($this->db->update($this->_table, array('status' => self::STATUS_EXCLUIDO, 'data_atualizacao' => $data_atualizacao))){
				$this->status = self::STATUS_EXCLUIDO;
				$this->data_atualizacao = $data_atualizacao;
				return true;
			}else{
				return false;
			}
		}
		
		//If Debug is enabled, will show an error, otherwise, will return FALSE when a error occurs.
		public function save($ignore_null=false){
			//Initiates transaction.
			$this->db->trans_start();
			
			if(isset($this->id)){
				$this->data_atualizacao = date("Y-m-d H:i:s");
				$this->db->update($this->_table, $this->getAtributosEValores($ignore_null), array('id' => $this->id) );
			}else{
				$this->data_cadastro = date("Y-m-d H:i:s");
				if(!isset($this->status)) $this->status = self::STATUS_PADRAO;
				$this->id = parent::insert($this->getAtributosEValores($ignore_null));
			}
			$this->db->trans_complete();
			
			return $this->db->trans_status();
		}
		
		public function create($parameters){
			$parameters['data_cadastro'] = (isset($parameters['data_cadastro'])) ? $parameters['data_cadastro'] : date("Y-m-d H:i:s",time());
			
			$parameters['status'] = (isset($parameters['status'])) ? $parameters['status'] : self::STATUS_PADRAO;
			
			return parent::insert($parameters);
		}
		
		public function create_batch($parameters){
			return parent::insert_batch($this->_table, $parameters);
		}
		
		
		public function find_by_id($id){
			$this->db->where($this->_table . '.id', $id);
			return array_pop($this->find());
		}
		
		//O uso do parâmetro $parameters está depreciado, favor usar as funções da classe Active_Record do CodeIgniter para filtros, etc.
		public function find($parameters=array()){
			$limit = (isset($parameters['limit'])) ? $parameters['limit'] : null;
			$offset = (isset($parameters['offset'])) ? $parameters['offset'] : null;
			$where = (isset($parameters['where'])) ? $parameters['where'] : null;

			if(isset($parameters['order_by']))
				$this->db->order_by($parameters['order_by']);
			if(!isset($where['status']) && $this->_has_status_column())
				$this->db->where_in($this->_table . '.status', $this->get_default_status());
			
			return $this->db->get_where($this->_table, $where, $limit, $offset)->result(get_class($this));
		}
		
		public function all($params=array()){
			return $this->find($params);
		}

		//Helper function to search and count_search methods
		function like_format($column){ return "lower($column) LIKE ";}
		
		public function search($term, $columns){
			if(is_array($columns) && $term !== NULL){
				$where_array = array_map( array($this, 'like_format'), $columns);
				$where_string = implode(" '%$term%' OR ", $where_array) . " '%$term%'";
				$this->db->where("($where_string)");
			}
			return $this->find();
		}
		
		public function count_search($term, $columns){
			if(is_array($columns) && $term !== NULL){
				$where_array = array_map( array($this, 'like_format'), $columns);
				$where_string = implode(" '%$term%' OR ", $where_array) . " '%$term%'";
				$this->db->where("($where_string)");
			}
			return $this->count_all_results($this->_table);
		}
		
		public function count_by($parameters){

			if(isset($parameters['where']))
				$this->db->where($parameters['where']);
			
			if(!isset($parameters['where']['status']))
				$this->db->where_in($this->_table . '.status', $this->get_default_status());
			
			return $this->count_all_results($this->_table);
		}
		
		public function count_all(){
			if ($this->_has_status_column())
				$this->db->where_in($this->_table . '.status', $this->get_default_status());
			
			return $this->db->count_all($this->_table);
		}
		
		public function getAtributos($exibeAtributosPadrao=false){
			//Lazy loading
			if(!isset($this->atributos))
				$this->atributos = $this->db->list_fields($this->_table);

			
			if(!$exibeAtributosPadrao)
				$atributosParaExcluir = array_keys(get_class_vars(self::BASE_MODEL_CLASSNAME));
			else
				$atributosParaExcluir = array();
			
			return array_diff($this->atributos, $atributosParaExcluir);
		}
			
		//Carrega atributos dinâmicamente, combinado com o getAtributos novo, carrega dinâmicamente atributos a partir das colunas do BD.
		public function inicializaAtributos(){
			if(get_class($this) === self::BASE_MODEL_CLASSNAME)
				return;
			
			foreach($this->getAtributos() as $atributo){
				$this->{$atributo} = isset($this->{$atributo}) ? $this->{$atributo} : null;
			}
		}
		
		public function getAtributosEValores($ignore_null=false){
			$retorno = array();
			foreach($this->getAtributos() as $atributo){
				if(!is_null($this->$atributo) || !$ignore_null)
					$retorno[$atributo] = $this->$atributo;
			}
			return $retorno;
		}
		
		protected function verificaAtributo($atributo){
			$atributos = $this->getAtributos();
			
			if(in_array($atributo, $atributos))
				return true;
			else
				return false;
		}
		
		public function atualizaAtributos($parametros=array()){
			foreach($parametros as $key => $value){
				if($this->verificaAtributo($key)){
					$this->$key = $value;
				}
			}			
	    }
			
		public function __call($method, $parameters){

			/* 
			 * Enables use o find_by_<attribute>_and_<another attribute>_and_<...> Ex: find_by_email_and_name('email@domain.com', 'Name');
			 * 
			 * Enables use o find_all_by_<attribute>_and_<another attribute>_and_<...> Ex: find_all_by_category_and_birthday('client', '1980-01-12');
			 * 
			 * The difference between find_by and find_all_by is what´s returned, find_by returns the first element found and find_all_by
			 * returns all the elements found.
			 * 
			 * Optional paramaters such as 'order_by' and 'limit' are expected to be passed as an hash, 
			 * wich is merged with the other parameters before the find method is called.
			 * 
			 */
			if(preg_match('/^find_by_|^find_all_by_/', $method)){
				$where_parameters = $parameters;
				$other_params = array();
				
				foreach($where_parameters as $i => $p){
					if(is_array($p)){
						$other_params = array_merge($other_params, $where_parameters[$i]);
						unset($where_parameters[$i]);
					}
				}
				
				//Recovers the query columns contained in the method name.
				$columns = explode('_and_', preg_replace('/^find_by_|^find_all_by_/', '', $method));

				$params = $this->parse_where_parameters($columns, $where_parameters);
				
				$result = $this->find(array_merge($other_params, $params));

				return preg_match('/^find_all_by_/', $method) ? $result : array_shift($result);
			}
			
			//Count_by_...
			if(preg_match('/^count_by_/', $method)){
				//Recovers the query columns contained in the method name.
				$columns = explode('_and_', preg_replace('/^count_by_/', '', $method));
				
				$params = $this->parse_where_parameters($columns, $parameters);
				
				return $this->count_by($params);
			}
			
			if(method_exists($this->db, $method)){
				return call_user_func_array(array($this->db, $method), $parameters);
			}

			return parent::__call($method, $parameters);
		}		
		
		//Adaptações para funcionar em PHP v.5.2

		function where_not($e){ return preg_replace('/_not$/', ' !=', $e);}
		
		function where_sort($a, $b){  return preg_match('/_is_null$|_is_not_null$/', $b); }
		
		function where_is_null($e){ return preg_replace('/_is_null$/', ' is NULL', $e);}
		
		function where_is_not_null($e){ return preg_replace('/_is_not_null$/', ' is not NULL', $e);}
		
		//Fim adaptações
		
		public function parse_where_parameters($columns, $parameters){
			
			$columns = array_map( array($this, 'where_not'), $columns);
				
			//Check if there is any column that does not receive parameters. 
			$arr_diff = count($columns) - count($parameters);
			
			if($arr_diff > 0){
				//Shifts columns that don´t receive a parameter to the end so combining columns and parameters will work fine.
				usort($columns, array($this, 'where_sort') );
				$columns = array_reverse($columns);
				
				//Replaces the sufix _is_null to the corresponding value in Codeigniter´s Active Record implementation
				$columns = array_map( array($this, 'where_is_null'), $columns);
				//Replaces the sufix _is_not_null to the corresponding value in Codeigniter´s Active Record implementation
				$columns = array_map( array($this, 'where_is_not_null'), $columns);
				
				$parameters = array_pad($parameters, ++$arr_diff, null);

			}

			//Combine column names with passed values to create the 'where' hash.
			$params = array('where' => array_combine($columns, array_slice($parameters, 0, count($columns))));
			
			//Recovers optional params such as 'limit' and 'order_by'.
			$optional_params = array_pop(array_slice($parameters, count($columns), 1));
			
			//Check if there is any optional parameter to be merged.
			if(is_array($optional_params))
				$params = array_merge($params, $optional_params);
			
			return $params;
		}
		
		public function get_status_validos(){
			return $this->status_validos;
		}
		
		public function get_status_dropdown_values(){
			foreach( $this->status_validos as $value ){
				$values[$value] = $value;
			}
			
			return $values;
		}

		public function dropdown_values($attribute, $default_option=NULL, $where=NULL){
			$values = ($default_option != NULL) ? $default_option : array();
			
			if($where !== NULL)
				$this->db->where($where);
			
			foreach( $this->all() as $value ){
				$values[$value->id] = $value->$attribute;
			}
			
			return $values;
		}
		
		public function __get($name){
			if(isset($this->belongs_to)){
				//Treats 1 or more "belongs_to"
				$params = (array_key_exists('belongs_to', $this->belongs_to)) ? array($this->belongs_to) : $this->belongs_to; 
				
				foreach($params as $belongs_to){
					$attribute = str_replace('_id', '', $belongs_to['column']);
					if($name == $attribute){
						//Lazy loading attribute.
						$this->{$attribute} = $this->{$belongs_to['belongs_to']}->find_by_id($this->{$belongs_to['column']});
						return $this->{$attribute};
					}
				}
			}
			
			if(isset($this->has_many)){
				//Treats 1 or more "$has_many"
				$params = (array_key_exists('has_many', $this->has_many)) ? array($this->has_many) : $this->has_many; 
				
				foreach($params as $has_many){
					$attribute = str_replace('_m', '', $has_many['has_many']);
					if($name == $attribute){
						$order_by = isset($has_many['order_by']) ? $has_many['order_by'] : 'id';
						$order_by_sort = isset($has_many['order_by_sort']) ? $has_many['order_by_sort'] : '';
						//Lazy loading attribute.
						$this->{$attribute} = $this->{$has_many['has_many']}->order_by($order_by, $order_by_sort)->{'find_all_by_' . $has_many['column']}($this->id);
						return $this->{$attribute};
					}
				}
			}
			
			if(isset($this->has_one)){
				//Treats 1 or more "$has_one"
				$params = (array_key_exists('has_many', $this->has_one)) ? array($this->has_one) : $this->has_one; 
				
				foreach($params as $has_one){
					$attribute = str_replace('_m', '', $has_one['has_one']);
					if($name == $attribute){
						$order_by = isset($has_one['order_by']) ? $has_one['order_by'] : 'id';
						$order_by_sort = isset($has_one['order_by_sort']) ? $has_one['order_by_sort'] : '';
						//Lazy loading attribute.
						$this->{$attribute} = $this->{$has_one['has_one']}->order_by($order_by, $order_by_sort)->{'find_by_' . $has_one['column']}($this->id);
						return $this->{$attribute};
					}
				}
			}
			
			return parent::__get($name);
		}
		
		//Filters records based on the status of who they belong to.
		public function parent_filter(){
			if(isset($this->belongs_to)){
				$model = $this->{$this->belongs_to['belongs_to']};	
				$table_name = $model->_table;
				$column = $this->belongs_to['column'];
				$attribute = str_replace('_id', '', $this->belongs_to['column']);
				
				$this->db->select($this->_table . '.*');
				$this->db->join($table_name, "$table_name.id = " . $this->_table . ".$column", 'right');
				$this->db->where_in($table_name . '.status', $model->get_default_status());
			}
			return $this;
		}

}
?>